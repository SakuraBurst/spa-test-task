import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { get } from 'lodash';
import { useSelector } from 'react-redux';

export default function PrivateRoute({ component: RouteComponent, ...rest }) {
  const data = useSelector(state => get(state, 'auth.data', {}));
  return (
    <Route
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
      render={({ location }) =>
        data.id ? (
          <RouteComponent />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}
PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired
};
