import React from 'react';
import { Switch, Route } from 'react-router-dom';
import LoginPage from './pages/Login';
import TodoPage from './pages/Todo';
import Template from './components/Template';
import PrivateRoute from './components/PrivateRoute';
import AlbumsPage from './pages/albums';
import HomePage from './pages/Home';

function App() {
  return (
    <Template>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/login" component={LoginPage} />
        <PrivateRoute path="/todo" component={TodoPage} />
        <PrivateRoute path="/albums" component={AlbumsPage} />
      </Switch>
    </Template>
  );
}

export default App;
/* not sure 


*/
