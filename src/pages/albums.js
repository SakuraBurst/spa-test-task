import React, { useEffect } from 'react';
import { ListGroup, Spinner, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { get, map } from 'lodash';
import {
  albumFetchAction,
  filterAlbum,
  getAlbumPhotos
} from '../actions/albums';
import MyModal from '../containers/modal';

function AlbumsPage() {
  const dispatch = useDispatch();
  const loading = useSelector(state => get(state, 'albums.loading', false));
  const data = useSelector(state => get(state, 'albums.data', []));
  const path = useSelector(state => get(state, 'router.location.pathname'));
  const [modalShow, setModalShow] = React.useState(false);
  useEffect(() => {
    localStorage.setItem('path', path);
    dispatch(albumFetchAction());
  }, [dispatch]);
  const getAlbum = id => {
    dispatch(getAlbumPhotos(id));
  };
  const filterAlbums = () => {
    dispatch(filterAlbum());
  };
  return (
    <ListGroup>
      {loading ? (
        <Spinner animation="border" />
      ) : (
        <div>
          <Button
            onClick={filterAlbums}
            style={{ marginBottom: '7px', marginLeft: '3px' }}
          >
            filterByName
          </Button>

          {map(data, item => (
            <ListGroup.Item
              style={{ cursor: 'pointer' }}
              onClick={() => {
                setModalShow(true);
                getAlbum(item.id);
              }}
            >
              {item.title}
            </ListGroup.Item>
          ))}
          <MyModal show={modalShow} onHide={() => setModalShow(false)} />
        </div>
      )}
    </ListGroup>
  );
}

export default AlbumsPage;
